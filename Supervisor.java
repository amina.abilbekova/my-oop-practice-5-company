package ITcompany;

import java.util.List;

public class Supervisor extends Employee{ //class to control the application of user choice
    private static FromRepository rep;

    public Supervisor(FromRepository repo) {
        super();
        this.rep = rep;
    }

    public Supervisor() {

    }
    public static String addEmployee(String fname, String lname, String salary) {
        Employee user = new Employee(fname, lname, salary);

        boolean created = rep.addEmployee(user); //checking the creation

        return (created ? "Employee was added!" : "The action was failed!");
    }

    public static String getEmployee(int id) {
        Employee user = rep.getEmployee(id);

        return (user == null ? "Employee with this id was not found!" : user.toString());
    }

    public static String getAllEmployee() {
        List<Employee> users = rep.getAllEmployee();

        return users.toString();
    }
}
