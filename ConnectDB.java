package ITcompany;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectDB { //connection to PostgreSql
    public ConnectDB() {

    }

    public Connection getConnection() throws SQLException, ClassNotFoundException {
        String connectionUrl = "jdbc:postgresql://localhost:5432/postgres";
        try {
            Class.forName("org.postgresql.Driver");

            Connection con = DriverManager.getConnection(connectionUrl, "postgres", "popytka123");

            return con;
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }
}
