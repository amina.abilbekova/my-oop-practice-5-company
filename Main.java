package ITcompany;

import ITcompany.*;
import java.sql.*;

public class Main {
    public static void main(String[] args) {
//calling and running
        ConnectDB db = new ConnectDB();
        FromRepository rep = new FromRepository(db);
        Supervisor controller = new Supervisor(rep);
        Menu app = new Menu(controller);
        app.start();
    }
}
