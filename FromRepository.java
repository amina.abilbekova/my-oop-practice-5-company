package ITcompany;
import Example.IDB;
import ITcompany.Employee;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class FromRepository extends Employee {

    private final ConnectDB db;

    public FromRepository(ConnectDB db) {
        super();
        this.db = db;
    }

    public boolean addEmployee(Employee user) { //adding new employee into the table
        Connection con = null;
        try {
            con = db.getConnection();
            String sql = "INSERT INTO employee(fname,lname,salary) VALUES (?,?,?)";
            PreparedStatement st = con.prepareStatement(sql);

            st.setString(1, user.getFname());
            st.setString(2, user.getLname());
            st.setInt(3, user.getSalary());

            st.execute();
            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return false;
    }
    public Employee getEmployee(int id) { //select single row employee's data by id
        Connection con = null;
        try {
            con = db.getConnection();
            String sql = "SELECT id,fname,lname,salary FROM employee WHERE id=?";
            PreparedStatement st = con.prepareStatement(sql);

            st.setInt(1, id);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Employee user = new Employee(rs.getInt("id"),
                        rs.getString("fname"),
                        rs.getString("lname"),
                        rs.getInt("salary"));

                return user;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return null;
    }

    public List<Employee> getAllEmployee() { //getting all data from table employee
        Connection con = null;
        try {
            con = db.getConnection();
            String sql = "SELECT id,fname,lname,salary FROM employee";
            Statement st = con.createStatement();

            ResultSet rs = st.executeQuery(sql);
            List<Employee> users = new LinkedList<>();
            while (rs.next()) {
                Employee user = new Employee(rs.getInt("id"),
                        rs.getString("fname"),
                        rs.getString("lname"),
                        rs.getInt("salary"));

                users.add(user);
            }

            return users;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return null;
    }
}
