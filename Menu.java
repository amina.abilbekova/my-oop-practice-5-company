package ITcompany;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Menu extends Supervisor{
    private Scanner scanner;

    public Menu() {
        super();
        scanner = new Scanner(System.in);
    }

    public Menu( Scanner scanner) {
        this.scanner = scanner;
    }

    public Menu(Supervisor controller) {

    }

    public void start() { //menu of application
        System.out.println();
        System.out.println("Choose one of the options:");
        System.out.println("1.Select all employee");
        System.out.println("2.Select employee by id");
        System.out.println("3.Add employee");
        System.out.println();
        try {
            int option = scanner.nextInt();
            //comparison statements to choose application
            if (option == 1) {
                getAllEmployee();
            } else if (option == 2) {
                getEmployeeById();
            } else if (option == 3) {
                addEmployee();
            }
        } catch (InputMismatchException _) {
            System.out.println("Input must be integer");
            scanner.nextLine();
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static String getAllEmployee() {
        String response = Supervisor.getAllEmployee();
        System.out.println(response);
        return response;
    }

    public void getEmployeeById() {
        System.out.println("Please enter id");

        int id = scanner.nextInt();
        String response = Supervisor.getEmployee(id);
        System.out.println(response);
    }

    public void addEmployee() {
        System.out.println("Please enter name");
        String name = scanner.next();
        System.out.println("Please enter surname");
        String surname = scanner.next();
        System.out.println("Please enter the salary");
        String salary = scanner.next();

        String response = Supervisor.addEmployee(fname, lname, salary);
        System.out.println(response);
    }
}
